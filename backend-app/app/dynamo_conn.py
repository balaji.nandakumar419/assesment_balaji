import boto3
import json
import logging
import os,random
from collections import defaultdict

# create a DynamoDB client using boto3. The boto3 library will automatically
# use the credentials associated with our ECS task role to communicate with
# DynamoDB, so no credentials need to be stored/managed at all by our code!
client = boto3.client('dynamodb',region_name='us-east-1')
#table_name = os.environ['DDB_TABLE_NAME']
table_name="vistors_log"

def update_vistor_number(ls_vistor):

    # Use the DynamoDB API UpdateItem to set the value of the mysfit's
    # Adopted attribute to True using an UpdateExpression.
    ls_vistor=int(ls_vistor)+1
    response =  client.update_item( TableName=table_name, Key={'vistors_id': { 'S': 'last_visitor' }},  AttributeUpdates={'count':{'Value':{  'N': "%s"%(ls_vistor)}} } )
          

    response = {}
    response["Update"] = str(ls_vistor);

    return json.dumps(response)
    
def get_visitor_number():
    
    response = client.get_item( TableName=table_name, Key={'vistors_id': { 'S': 'last_visitor' }},  AttributesToGet=["count"] )
    ls_vistor=response['Item']['count']['N']
    response=update_vistor_number(ls_vistor)
    return(response)
