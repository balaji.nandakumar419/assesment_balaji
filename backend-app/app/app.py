from flask import Flask,request
from flask import render_template
import socket,requests
import random,ast,dynamo_conn
import os
import argparse

app = Flask(__name__)

color_codes = {
    "red": "#e74c3c",
    "green": "#16a085",
    "blue": "#2980b9",
    "blue2": "#30336b",
    "pink": "#be2edd",
    "default-db": "#130f40"
}

SUPPORTED_COLORS = ",".join(color_codes.keys())

# Get color from Environment variable
COLOR_FROM_ENV = os.environ.get('APP_COLOR')
# Generate a random color
COLOR = random.choice(["red", "green", "blue", "blue2", "darkblue", "pink"])


@app.route('/get_data', methods=['GET', 'POST'])
def parse_data():
  if request.method == "POST":
    print("inside POST method")
  elif request.method == "GET":
    print("inside GET method")
    response=dynamo_conn.get_visitor_number()
    return response  
    #data = request.data
		
@app.route('/health_check', methods=['GET'])
def main():
  return "App is heathy"

if __name__ == "__main__":
  app.run(host="0.0.0.0", port=8080)
