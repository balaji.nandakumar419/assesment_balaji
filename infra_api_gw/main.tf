module "bals_vpc" {
    #source = "./modules/vpc"
    source= "git@gitlab.com:balaji.nandakumar419/terralearning.git//modules/vpc"
    region = var.region
    module_tags = var.module_tags
    cidr_range = var.cidr_range
    public_subnet_cidr = var.public_subnet_cidr
    private_subnet_cidr = var.private_subnet_cidr
    availability_zone = var.availability_zone
}
