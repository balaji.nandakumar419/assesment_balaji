resource "aws_ecs_cluster" "front-end" {
  name = "front-end-app"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster" "back-end" {
  name = "back-end-app"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}


resource "aws_cloudwatch_log_group" "front-end-ecs-cw" {
  name = "front-end-ecs-cw"
}



resource "aws_cloudwatch_log_group" "back-end-ecs-cw" {
  name = "back-end-ecs-cw"
}
