locals {
  target_groups = "${tolist([tomap({"name" = "first-app-tg", "backend_protocol"="HTTP", "backend_port"="8080", "target_type"="ip", "health_check_timeout"="10", "health_check_interval"="15", "health_check_path"="/health_check", "health_check_matcher"="200"}),tomap({"name"="second-app-tg", "backend_protocol"="HTTP", "backend_port"="8080", "target_type"="ip", "health_check_timeout"="10", "health_check_interval"="15", "health_check_path"="/health_check", "health_check_matcher"="200"})])}"
  target_groups_defaults="${tomap({"name" = "first-app-tg", "backend_protocol"="HTTP", "backend_port"="8080", "target_type"="ip", "health_check_timeout"="10", "health_check_interval"="15", "health_check_path"="/health_check", "health_check_matcher"="200"})}"
}
resource "aws_lb" "click_app" {
  load_balancer_type               = "application"
  name                             = var.load_balancer_name
  internal                         = var.load_balancer_is_internal
  security_groups                  = [aws_security_group.elb_private_sec_group.id]
  subnets                          = module.bals_vpc.private_subnet_id
  idle_timeout                     = var.idle_timeout
  tags = merge(
    var.module_tags,
    {
      "Name" = var.load_balancer_name
    },
  )

  access_logs {
    enabled = false
    bucket  = var.log_bucket_name
    prefix  = var.log_location_prefix
  }


  count = var.create_alb && var.logging_enabled ? 1 : 0
}





resource "aws_lb_target_group" "app-tg" {
  name     = local.target_groups[count.index]["name"]
  vpc_id   = module.bals_vpc.vpc_id
  port     = local.target_groups[count.index]["backend_port"]
  protocol = upper(local.target_groups[count.index]["backend_protocol"])
  target_type = lookup(
    local.target_groups[count.index],
    "target_type",
    local.target_groups_defaults["target_type"],
  )
  health_check {
    interval = lookup(
      local.target_groups[count.index],
      "health_check_interval",
      local.target_groups_defaults["health_check_interval"],
    )
    path = lookup(
      local.target_groups[count.index],
      "health_check_path",
      local.target_groups_defaults["health_check_path"],
    )

    timeout = lookup(
      local.target_groups[count.index],
      "health_check_timeout",
      local.target_groups_defaults["health_check_timeout"],
    )

    matcher = lookup(
      local.target_groups[count.index],
      "health_check_matcher",
      local.target_groups_defaults["health_check_matcher"],
    )
  }



  tags = merge(
    var.module_tags,
    {
      "Name" = local.target_groups[count.index]["name"]
    },
  )
  count      = var.create_alb && var.logging_enabled ? var.target_groups_count : 0
  depends_on = [aws_lb.click_app]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "frontend_http" {
  load_balancer_arn = aws_lb.click_app[0].arn
  port     = 80
  protocol = "HTTP"
  count    = var.create_alb && var.logging_enabled ? var.http_tcp_listeners_count : 0

  default_action {
    target_group_arn = aws_lb_target_group.app-tg[0].id
    type             = "forward"
  }
} 



resource "aws_lb_listener_rule" "parse_data_rule" {
  listener_arn = aws_lb_listener.frontend_http[0].arn
  priority     = 98

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-tg[0].arn
  }

  condition {
    path_pattern {
      values = ["/parse_data"]
    }
  }
}


resource "aws_lb_listener_rule" "get_data_rule" {
  listener_arn = aws_lb_listener.frontend_http[0].arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-tg[1].arn
  }

  condition {
    path_pattern {
      values = ["/get_data"]
    }
  }
}
