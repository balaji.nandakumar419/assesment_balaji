resource "aws_security_group" "ecs_private_sec_group" {
   name        = "ecs_private_sec_group"
   description = "Security group for ecs tasks"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }

 resource "aws_security_group" "elb_private_sec_group" {
   name        = "elb_private_sec_group"
   description = "Security group for ecs tasks"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }


 resource "aws_security_group" "vpc_link_sec_group" {
   name        = "vpc_link_sec_group"
   description = "Security group for ecs tasks"
   vpc_id      = module.bals_vpc.vpc_id

   tags = var.module_tags
 }


 resource "aws_security_group_rule" "outb_ecs" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_private_sec_group.id
}

 resource "aws_security_group_rule" "outb_private_link" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_link_sec_group.id
}

 resource "aws_security_group_rule" "outb_elb" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.elb_private_sec_group.id
}


 resource "aws_security_group_rule" "inb_elb" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  source_security_group_id = aws_security_group.vpc_link_sec_group.id
  security_group_id = aws_security_group.elb_private_sec_group.id
}

resource "aws_security_group_rule" "inb_private_link" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.vpc_link_sec_group.id
}

resource "aws_security_group_rule" "inb_ecs" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_private_sec_group.id
  source_security_group_id = aws_security_group.elb_private_sec_group.id
}

 resource "aws_security_group_rule" "inb_elb_open_vpc" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [var.cidr_range]
  security_group_id = aws_security_group.elb_private_sec_group.id
}
