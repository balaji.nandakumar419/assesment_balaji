output "elb_target_group_arn" {
  value = aws_lb_target_group.app-tg.*.id
}

output "elb_cname" {
  value = aws_lb.click_app[0].dns_name
}

output "elb_arn_suffix" {
  value = aws_lb.click_app[0].arn_suffix
}
