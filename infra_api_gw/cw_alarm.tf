locals {
target_group_id = aws_lb_target_group.app-tg.*.arn_suffix
load_balancer_id = aws_lb.click_app[0].arn_suffix
actions_alarm = [aws_sns_topic.app-alarm.arn]
actions_ok = [aws_sns_topic.app-alarm.arn]
}



resource "aws_cloudwatch_metric_alarm" "front_end_httpcode_target_5xx_count" {
  alarm_name          = "front_end_alb-tg-${local.target_group_id[0]}-high5XXCount"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "HTTPCode_Target_5XX_Count"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Sum"
  threshold           = "0"
  alarm_description   = "Average API 5XX target group error code count is too high"
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "TargetGroup"  = local.target_group_id[0]
    "LoadBalancer" = local.load_balancer_id
  }
}

resource "aws_cloudwatch_metric_alarm" "backend_end_httpcode_target_5xx_count" {
  alarm_name          = "back_end_alb-tg-${local.target_group_id[1]}-high5XXCount"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "HTTPCode_Target_5XX_Count"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Sum"
  threshold           = "0"
  alarm_description   = "Average API 5XX target group error code count is too high"
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "TargetGroup"  = local.target_group_id[1]
    "LoadBalancer" = local.load_balancer_id
  }
}

resource "aws_cloudwatch_metric_alarm" "alb_httpcode_lb_5xx_count" {
  alarm_name          = "alb-${local.load_balancer_id}-high5XXCount"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "HTTPCode_ELB_5XX_Count"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Sum"
  threshold           = "0"
  alarm_description   = "Average API 5XX load balancer error code count is too high"
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "LoadBalancer" = local.load_balancer_id
  }
}

resource "aws_cloudwatch_metric_alarm" "front_end_target_response_time_average" {
  alarm_name          = "front_end_alb-tg-${local.target_group_id[0]}-highResponseTime"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "TargetResponseTime"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Average"
  threshold           = var.response_time_threshold
  alarm_description   = format("Average API response time is greater than %s", var.response_time_threshold)
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "TargetGroup"  = local.target_group_id[0]
    "LoadBalancer" = local.load_balancer_id
  }
}

resource "aws_cloudwatch_metric_alarm" "back_end_target_response_time_average" {
  alarm_name          = "back_end_alb-tg-${local.target_group_id[0]}-highResponseTime"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "TargetResponseTime"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Average"
  threshold           = var.response_time_threshold
  alarm_description   = format("Average API response time is greater than %s", var.response_time_threshold)
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "TargetGroup"  = local.target_group_id[0]
    "LoadBalancer" = local.load_balancer_id
  }
}

resource "aws_cloudwatch_metric_alarm" "front_end_unhealthy_hosts" {
  alarm_name          = "front_end_alb-tg-${local.target_group_id[0]}-unhealthy-hosts"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "UnHealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Minimum"
  threshold           = var.unhealthy_hosts_threshold
  alarm_description   = format("Unhealthy host count is greater than %s", var.unhealthy_hosts_threshold)
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "TargetGroup"  = local.target_group_id[0]
    "LoadBalancer" = local.load_balancer_id
  }
}

resource "aws_cloudwatch_metric_alarm" "back_end_unhealthy_hosts" {
  alarm_name          = "back_end_alb-tg-${local.target_group_id[1]}-unhealthy-hosts"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "UnHealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  period              = var.statistic_period
  statistic           = "Minimum"
  threshold           = var.unhealthy_hosts_threshold
  alarm_description   = format("Unhealthy host count is greater than %s", var.unhealthy_hosts_threshold)
  alarm_actions       = local.actions_alarm
  ok_actions          = local.actions_ok

  dimensions = {
    "TargetGroup"  = local.target_group_id[1]
    "LoadBalancer" = local.load_balancer_id
  }
}
