terraform {
  backend "s3" {
    bucket = "bals-infra-files"
    key    = "ecs_infra.tf"
    region = "us-east-1"
    #role_arn = var.assume_role
  }
}
